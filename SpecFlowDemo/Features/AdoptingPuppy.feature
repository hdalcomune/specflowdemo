﻿Feature: Adopting puppies
	As a puppy lover 
	I want to adopt puppies
	So they can be play together

@mytag
Scenario: Adopting a puppy
	Given I am on the puppy adoption site
	When I view the details of the first dog
	And I proceed to adoption
	And I enter "Rick" in the name field
	And I enter "221b Baker Street" in the address field
	And I enter "puppy_doctor@puppies.com" in the email field
	And I select "Credit card" from the payment dropdown
	Then I should see the message "Thank you for adopting a puppy!"

Scenario: Adopting two puppies
Given I am on the puppy adoption site
	When I view the details of the first dog
	And I choose to adopt
	But I select the second puppy
	And I choose to adopt
	Then I should see two puppies added to my litter

Scenario Outline: Adopting puppy with different payment methods
	Given I am on the puppy adoption site
	When I view the details of the first dog
	And I proceed to adoption
	And I enter "<name>" in the name field
	And I enter "<address>" in the address field
	And I enter "<email>" in the email field
	And I select "<payment>" from the payment dropdown
	Then I should see the message "Thank you for adopting a puppy!"

	Examples: 
	| name  | address               | email             | payment        |
	| Homer | 742 Evergreen Terrace | homer@simpson.com | Purchase order |
	| Peter | 31 Spooner Street     | peter@griffin.com | Check          |