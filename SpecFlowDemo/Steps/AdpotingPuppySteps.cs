﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace SpecFlowDemo.Steps
{
    [Binding]
    public class AdpotingPuppySteps
    {
        public IWebDriver driver;

        [Given(@"I am on the puppy adoption site")]
        public void GivenIAmOnThePuppyAdoptionSite()
        {
            driver = new ChromeDriver();
            driver.Url = "http://puppies.herokuapp.com";
        }
        
        [When(@"I view the details of the first dog")]
        public void WhenIViewTheDetailsOfTheFirstDog()
        {
            driver.FindElement(By.ClassName("rounded_button")).Click();
        }

        [When(@"I proceed to adoption")]
        public void WhenIProceedToAdoption()
        {
            driver.FindElement(By.ClassName("rounded_button")).Click();
            driver.FindElement(By.ClassName("rounded_button")).Click();
        }

        [When(@"I enter ""(.*)"" in the name field")]
        public void WhenIEnterInTheNameField(string name)
        {
            driver.FindElement(By.Id("order_name")).SendKeys(name);
        }
        
        [When(@"I enter ""(.*)"" in the address field")]
        public void WhenIEnterInTheAddressField(string address)
        {
            driver.FindElement(By.Id("order_address")).SendKeys(address);
        }

        [When(@"I enter ""(.*)"" in the email field")]
        public void WhenIEnterInTheEmailField(string email)
        {
            driver.FindElement(By.Id("order_email")).SendKeys(email);
        }

        [When(@"I select ""(.*)"" from the payment dropdown")]
        public void WhenISelectFromThePaymentDropdown(string payment)
        {
            var element = driver.FindElement(By.Id("order_pay_type"));
            var selectElement = new SelectElement(element);
            selectElement.SelectByText(payment);
        }
        
        [Then(@"I should see the message ""(.*)""")]
        public void ThenIShouldSeeTheMessage(string message)
        {
            driver.FindElement(By.Name("commit")).Click();
            Assert.IsTrue(driver.PageSource.Contains(message));
        }

        [When(@"I choose to adopt")]
        public void WhenIChooseToAdopt()
        {
            driver.FindElement(By.ClassName("rounded_button")).Click();
        }

        [When(@"I select the second puppy")]
        public void WhenISelectTheSecondPuppy()
        {
            driver.FindElement(By.XPath("//*[@id='content']/div[2]/form[2]/div/input[1]")).Click();
            driver.FindElement(By.XPath("//*[@id='content']/div[3]/div/div[4]/form/div/input")).Click();
        }

        [Then(@"I should see two puppies added to my litter")]
        public void ThenIShouldSeeTwoPuppiesAddedToMyLitter()
        {
            IList<IWebElement> elements = driver.FindElements(By.ClassName("item_price"));
            Assert.That(elements.Count == 2, "could not see two puppies");
        }

    }
}
