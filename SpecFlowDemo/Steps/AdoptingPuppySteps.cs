﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowDemo.Steps
{
    [Binding]
    public class AdoptingPuppySteps
    {
        public IWebDriver driver;

        [Given(@"I am on the puppy adoption site")]
        public void GivenIAmOnThePuppyAdoptionSite()
        {
            driver = TestBase.driver;
        }
        
        [When(@"I view the details of the first dog")]
        public void WhenIViewTheDetailsOfTheFirstDog()
        {
            driver.FindElement(By.ClassName("rounded_button")).Click();

        }

        [When(@"I proceed to adoption")]
        public void WhenIProceedToAdoption()
        {
            //driver.FindElement(By.Name("Adopt Me!")).Click();
            driver.FindElement(By.ClassName("rounded_button")).Click();
            driver.FindElement(By.ClassName("rounded_button")).Click();
        }

        [When(@"I enter ""(.*)"" in the name field")]
        public void WhenIEnterInTheNameField(string name)
        {
            driver.FindElement(By.Id("order_name")).SendKeys(name);
        }
        
        [When(@"I enter ""(.*)"" in the address field")]
        public void WhenIEnterInTheAddressField(string address)
        {
            driver.FindElement(By.Id("order_address")).SendKeys(address);
        }

        [When(@"I enter ""(.*)"" in the email field")]
        public void WhenIEnterInTheEmailField(string email)
        {
            driver.FindElement(By.Id("order_email")).SendKeys(email);
        }

        [When(@"I select ""(.*)"" from the payment dropdown")]
        public void WhenISelectFromThePaymentDropdown(string payment)
        {
            var element = driver.FindElement(By.Id("order_pay_type"));
            var selectElement = new SelectElement(element);
            selectElement.SelectByText(payment);
        }
        
        [Then(@"I should see the message ""(.*)""")]
        public void ThenIShouldSeeTheMessage(string message)
        {
            driver.FindElement(By.Name("commit")).Click();
            Assert.IsTrue(driver.PageSource.Contains(message));
        }
    }
}
